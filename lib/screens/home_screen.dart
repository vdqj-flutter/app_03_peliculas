import 'package:app_03_peliculas/providers/providers.dart';
import 'package:flutter/material.dart';

import 'package:app_03_peliculas/widgets/widgets.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatelessWidget {
  // const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final moviesProvider =
        Provider.of<MoviesProvider>(context); // servicio de peliculas
    //print('prov: ${moviesProvider.onDisplayMovies}');

    return Scaffold(
      appBar: AppBar(
        title: Text('Home Screen'),
        elevation: 0,
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.search_outlined),
          )
        ],
      ),
      body: SingleChildScrollView(
          child: Column(children: [
        // Tarjetas principal
        CardSwiper(movies: moviesProvider.onDisplayMovies),

        // Slider de peliculas
        MovieSlider(
          movies: moviesProvider.onPopularMovies,
          title: 'Populares', //  title es opcional
          onNextPage: () => moviesProvider.findAll_moviePopulares(),
        ),
      ])),
    );
  }
}

// SingleChildScrollView: permite hacer scroll si el contenido pasa

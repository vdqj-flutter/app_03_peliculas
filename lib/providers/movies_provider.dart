import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;

import 'package:app_03_peliculas/models/models.dart';

class MoviesProvider extends ChangeNotifier {
  String _baseUrl = 'api.themoviedb.org'; // 3/movie/now_playing
  String _language = 'en-US';
  final Map<String, String> headers = {
    'Authorization':
        'Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIxYTlhZTZjZTJlYjQ4ZThkMTg3OWEyMWM3M2U3MTE4ZSIsInN1YiI6IjY1MjFkMGRkZWE4NGM3MDBhZWVkZWY2OCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.lL_NDxwbCD7EntDl_lfiGbkVEM37Q-LRK36Zedpw_K0',
  };
  List<Movie> onDisplayMovies = [];
  List<Movie> onPopularMovies = [];
  int _popularPage = 0;

  MoviesProvider() {
    print('Movies provider inicializado');
    this.findAll_movieNowPlaying();
    this.findAll_moviePopulares();
  }

  Future get_jsonData(String endpoint, [int page = 1]) async {
    var url = Uri.https(
        this._baseUrl, endpoint, {'language': this._language, 'page': '$page'});
    final response = await http.get(url, headers: headers);
    return response.body;
  }

  Future get_jsonDataNotParams(String endpoint) async {
    var url = Uri.https(this._baseUrl, endpoint);
    final response = await http.get(url, headers: headers);
    return response.body;
  }

  /// @Description: obtiene lista de peliculas
  /// url: https://developer.themoviedb.org/reference/movie-now-playing-list
  findAll_movieNowPlaying() async {
    final response = await get_jsonData('3/movie/now_playing');
    final newPlayingResponse = NowPlayingResponse.fromRawJson(response);
    onDisplayMovies = newPlayingResponse.results;
    notifyListeners(); // anuncia a los widgets que cambio un valor
  }

  /// @Description: obtiene lista de peliculas populares
  /// url: https://api.themoviedb.org/3/movie/popular?language=en-US&page=1
  findAll_moviePopulares() async {
    _popularPage++;
    final response = await get_jsonData('3/movie/popular', _popularPage);
    final popularResponse = PopularResponse.fromRawJson(response);
    onPopularMovies = [...onPopularMovies, ...popularResponse.results];
    notifyListeners(); // anuncia a los widgets que cambio un valor
  }

  /// @Description: obtiene lista de peliculas populares
  /// url: https://api.themoviedb.org/3/movie/popular?language=en-US&page=1
  findAll_movieCredits(int movieId) async {
    _popularPage++;
    final response = await get_jsonDataNotParams('3/movie/${movieId}/credits');
    final popularResponse = PopularResponse.fromRawJson(response);
    onPopularMovies = [...onPopularMovies, ...popularResponse.results];
    notifyListeners(); // anuncia a los widgets que cambio un valor
  }
}

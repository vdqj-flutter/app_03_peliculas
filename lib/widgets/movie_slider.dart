import 'package:app_03_peliculas/models/models.dart';
import 'package:flutter/material.dart';

class MovieSlider extends StatefulWidget {
  final List<Movie> movies; // no es opcional
  final String? title; // es opcional
  final Function onNextPage;

  const MovieSlider(
      {super.key, required this.movies, this.title, required this.onNextPage});

  @override
  State<MovieSlider> createState() => _MovieSliderState();
}

class _MovieSliderState extends State<MovieSlider> {
  final ScrollController scrollController = ScrollController();

  @override
  void initState() {
    // TODO: al inicio
    super.initState();
    scrollController.addListener(() {
      //print(scrollController.position.pixels); // posicion actual del scroll
      //print(scrollController.position.maxScrollExtent); //  fin del scroll
      if (scrollController.position.pixels >=
          scrollController.position.maxScrollExtent - 500) {
        //print('obtener pagina siguiente');
        widget.onNextPage();
      }
    });
  }

  @override
  void dispose() {
    // TODO: para destruir
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.movies.isEmpty) {
      return Container(
        width: double.infinity,
        height: 260,
        child: Center(child: CircularProgressIndicator()),
      );
    } else {
      return Container(
        width: double.infinity, // usa todo el ancho
        height: 260,
        // color: Colors.red,
        child: Column(
          crossAxisAlignment:
              CrossAxisAlignment.start, // acomoda el contenido al inicio izq
          children: [
            // Padding(
            //   padding: EdgeInsets.symmetric(horizontal: 20),
            //   child: Text(
            //     title ?? 'Populares',
            //     style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            //   ),
            // ),
            if (widget.title != null) // Verifica si title no es nulo
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Text(
                  widget.title!,
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
              ),
            SizedBox(height: 5),
            Expanded(
              child: ListView.builder(
                controller: scrollController,
                scrollDirection: Axis.horizontal,
                itemCount: widget.movies.isNotEmpty ? widget.movies.length : 20,
                itemBuilder: (_, int index) =>
                    _MoviPoster(movie: widget.movies[index]),
              ),
            ),
          ],
        ),
      );
    }
  }
}

class _MoviPoster extends StatelessWidget {
  final Movie movie;

  const _MoviPoster({super.key, required this.movie});
  // const _MoviPoster({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 130,
      height: 190,
      //color: Colors.green,
      margin: EdgeInsets.symmetric(horizontal: 10),
      child: Column(children: [
        GestureDetector(
          onTap: () =>
              Navigator.pushNamed(context, 'details', arguments: movie),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(20),
            child: FadeInImage(
              placeholder: AssetImage('assets/no-image.jpg'),
              // image: NetworkImage('https://via.placeholder.com/300x400'),
              image: NetworkImage(movie.fullPosterImage),
              width: 130, // ancho de la imagen
              height: 190, // alto de la imagen
              fit: BoxFit.cover, // imagen usa todo el ancho
            ),
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Text(
          movie.title ?? 'Sin title',
          maxLines: 2, // si el texto es largo, se hace un salto de linea
          overflow: TextOverflow.ellipsis, // pone puntos suspensivos si se pasa
          textAlign: TextAlign.center, // texto centrado
        )
      ]),
    );
  }
}

import 'package:flutter/material.dart';

import 'package:app_03_peliculas/models/models.dart';
import 'package:flutter_card_swipper/flutter_card_swiper.dart';

/// LISTA DE CARDS GRANDES
class CardSwiper extends StatelessWidget {
  final List<Movie> movies;

  const CardSwiper({super.key, required this.movies});

  // const CardSwiper({super.key, required this.movies});

  @override
  Widget build(BuildContext context) {
    final size =
        MediaQuery.of(context).size; // obtener el tamaño de la pantalla

    if (movies.isEmpty) {
      return Container(
        width: double.infinity,
        height: size.height * 0.5,
        child: Center(child: CircularProgressIndicator()),
      );
    } else {
      return Container(
        width: double.infinity,
        height: size.height *
            0.5, // 0.9 = 90% /// asignamos la mitad de la pantalla
        // color: Colors.red,
        child: Swiper(
          itemCount: (movies.isNotEmpty) ? movies.length : 10,
          // itemCount: movies.length,
          layout: SwiperLayout.STACK,
          itemWidth: size.width * 0.6,
          itemHeight: size.height * 0.4,
          itemBuilder: (_, int index) {
            final movie = movies[index];
            // print(movie.fullPosterImage);
            return GestureDetector(
              onTap: () =>
                  Navigator.pushNamed(context, 'details', arguments: movie),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: FadeInImage(
                  placeholder: AssetImage('assets/no-image.jpg'),
                  image: NetworkImage(movie.fullPosterImage),
                  fit: BoxFit.cover,
                ),
              ),
            );
          },
        ),
      );
    }
  }
}
